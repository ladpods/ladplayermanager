#import "LADVideoPlayerViewController.h"

// Category
#import "NSBundle+LADPlayerManagerAdditions.h"

// Configurations
#import "LADVideoPlayerConfiguration.h"
#import "LADVideoPlayerAnalyticsConfiguration.h"

// External
#import <JWPlayer_iOS_SDK/JWPlayerController.h>

// View
#import "LADVideoPlayerControlsView.h"

// ---------------------------------------------------------------------------------
#pragma mark - Interface


@interface LADVideoPlayerViewController (/* Private */) <JWPlayerDelegate, UIWebViewDelegate>


@property (nonatomic, strong) JWPlayerController                    *playerController;
@property (nonatomic, strong) LADVideoPlayerControlsView            *controlsView;
@property (nonatomic, strong) JWConfig                              *configuration;
@property (nonatomic, strong) LADVideoPlayerAnalyticsConfiguration  *analyticsConfiguration;

@property (nonatomic, weak)   UIView                                *jwFullScreenView;

// Action
//- (void)__panGestureAction:(UIPanGestureRecognizer *)sender;
- (void)__dismiss;

@end

// ---------------------------------------------------------------------------------
#pragma mark - Implementation


@implementation LADVideoPlayerViewController

// ---------------------------------------------------------------------------------
#pragma mark - Initialization


+ (instancetype)viewControllerWithConfiguration:(JWConfig *)configuration
                         analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration {
    
    return [[self alloc] initWithConfiguration:configuration
                        analyticsConfiguration:analyticsConfiguration];
}

// ---------------------------------------------------------------------------------

- (instancetype)initWithConfiguration:(JWConfig *)configuration
               analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration {
    
    self = [super init];
    
    if (self) {
        
        _configuration = configuration;
        _analyticsConfiguration = analyticsConfiguration;
        _allowRotation = TRUE;
        _presentationOrientation = UIInterfaceOrientationMaskLandscape;
        
    }
    
    return self;
}

// ---------------------------------------------------------------------------------
#pragma mark - Properties

- (JWPlayerController *)playerController {
    
    if (!_playerController) {
        
        _playerController = [[JWPlayerController alloc] initWithConfig:self.configuration
                                                              delegate:self];
        [_playerController enterFullScreen];
    }
    
    return _playerController;
}

// ---------------------------------------------------------------------------------

- (LADVideoPlayerControlsView *)controlsView {
    
    if (!_controlsView) {
        
        _controlsView = [[NSBundle lad_currentBundleForClass:[LADVideoPlayerControlsView class]]
                         loadNibNamed:NSStringFromClass([LADVideoPlayerControlsView class])
                         owner:self
                         options:nil][0];
        
        _controlsView.playerController = self.playerController;
        _controlsView.backgroundColor = [UIColor clearColor];
        _controlsView.hidden = YES;
        
        __weak __typeof__(self) weakSelf = self;
        
        _controlsView.onCloseAction = ^{
            __typeof__(self) strongSelf = weakSelf;
            
            [strongSelf __dismiss];
        };
        
        _controlsView.onStartChromecastAction = ^{
            __typeof__(self) strongSelf = weakSelf;
            
            if (strongSelf.analyticsConfiguration.onStartChromecast) {
                strongSelf.analyticsConfiguration.onStartChromecast(strongSelf.playerController);
            }
        };
        
        _controlsView.onStartAirplayAction = ^{
            __typeof__(self) strongSelf = weakSelf;
            
            if (strongSelf.analyticsConfiguration.onStartAirplay) {
                strongSelf.analyticsConfiguration.onStartAirplay(strongSelf.playerController);
            }
        };
        
        [self.playerController.view addSubview:_controlsView];
        
    }
    
    return _controlsView;
}

// ---------------------------------------------------------------------------------
#pragma mark - Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Add player
    [self.view addSubview:self.playerController.view];
    
}

// ---------------------------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];
}

// ---------------------------------------------------------------------------------

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    
    if (UIUserInterfaceIdiomPhone == UI_USER_INTERFACE_IDIOM()) {
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait)
                                    forKey:@"orientation"];
    }
}

// ---------------------------------------------------------------------------------

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    self.playerController.view.frame = CGRectMake(0,
                                                  0,
                                                  CGRectGetWidth(self.view.bounds),
                                                  CGRectGetHeight(self.view.bounds));
    
    int posY = 0.0;
    
    //iPhone X - case
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && UIScreen.mainScreen.nativeBounds.size.height == 2436)
    {
        
        posY = 20.0;
    }
    
    self.controlsView.frame = CGRectMake(0,
                                         posY,
                                         self.controlsView.currentWidth,
                                         50);
}

// ---------------------------------------------------------------------------------

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self jwPlayerUpdateLayout];
}

// ---------------------------------------------------------------------------------

- (void)jwPlayerUpdateLayout
{
    for (UIWindow * aWindow in [UIApplication sharedApplication].windows)
    {
#ifdef DEBUG
        //NSLog(@"Window debug : %@",aWindow.debugDescription);
        //NSLog(@"\n\nTEST root : %@\n\n\n\n",aWindow.rootViewController.debugDescription);
#endif
        NSString *strClass = NSStringFromClass([aWindow.rootViewController class]);
        
        if ([strClass isEqualToString:@"JWFullScreenViewController"])
        {
            // Fix main View layout in JWPlayer window instance, masterview of class 'JWFullScreenViewController'
            [self setJwFullScreenView:aWindow.rootViewController.view];
        }
    }
    
    [self performSelector:@selector(jwPlayerLayoutFix)
               withObject:nil
               afterDelay:1.f];
}

// ---------------------------------------------------------------------------------

- (void)jwPlayerLayoutFix
{
    // Fix Layout on view added on our masterView by JWPlayer
    UIView *jwInteractionView =  (UIView*)self.view.subviews[0];
    [jwInteractionView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    jwInteractionView.frame = self.playerController.view.frame;
    [jwInteractionView setNeedsLayout];
    
    // Fix main container on JWPlayer
    if (self.jwFullScreenView != nil)
    {
        if (self.jwFullScreenView.frame.size.width != [UIScreen mainScreen].bounds.size.width || self.jwFullScreenView.frame.size.height != [UIScreen mainScreen].bounds.size.height)
        {
            self.jwFullScreenView.frame = [UIScreen mainScreen].bounds;
            [self.jwFullScreenView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
            [self.jwFullScreenView setNeedsLayout];
        }
    }
}

// ---------------------------------------------------------------------------------
#pragma mark - Orientation

- (BOOL)shouldAutorotate {
    
    return _allowRotation;
}

// ---------------------------------------------------------------------------------

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    if (_allowRotation == FALSE) {
        return _presentationOrientation;
    }
    else{
        return UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskLandscape;
    }
}

// ---------------------------------------------------------------------------------

- (BOOL)prefersStatusBarHidden {
    return YES;
}

// ---------------------------------------------------------------------------------
#pragma mark - JWPlayerDelegate

- (void)onReady:(NSInteger)setupTime {
    self.controlsView.hidden = NO;
}

// ---------------------------------------------------------------------------------

- (void)onPlay:(NSString *)oldState {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onPlay) {
        
        self.analyticsConfiguration.onPlay(self.playerController);
    }
}

// ---------------------------------------------------------------------------------

- (void)onPause:(NSString *)oldState {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onPause) {
        
        self.analyticsConfiguration.onPause(self.playerController);
    }
}

// ---------------------------------------------------------------------------------

- (void)onComplete {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onStop) {
        
        self.analyticsConfiguration.onStop(self.playerController);
    }
    
    [self __dismiss];
}

// ---------------------------------------------------------------------------------

- (void)onSeek:(double)offset fromPosition:(double)position {
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onStartSeeking) {
        
        self.analyticsConfiguration.onStartSeeking(self.playerController);
    }
}

// ---------------------------------------------------------------------------------

- (void)onSeeked {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onStopSeeking) {
        
        self.analyticsConfiguration.onStopSeeking(self.playerController);
    }
}

// ---------------------------------------------------------------------------------

- (void)onControlBarVisible:(BOOL)isVisible {
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        //Run UI Updates
        //self.controlsView.hidden = !isVisible;
        self.controlsView.hidden = FALSE;
    });
    
}

// ---------------------------------------------------------------------------------

- (void)onError:(NSError *)error {
    self.controlsView.hidden = NO;
}

// ---------------------------------------------------------------------------------

- (void)onAdError:(NSError *)error{
    
    /// PATCH : video content not play on adError if tag is configure
    /// with an empty configuration
    
    // if we force a pause that relaunch content play, why don't know
    // may be a bug in jwplayer sdk
    [self.playerController pause];
}

// ---------------------------------------------------------------------------------
#pragma mark - Action

- (void)__dismiss {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onStop) {
        
        self.analyticsConfiguration.onStop(self.playerController);
    }
    
    if (self.playerController.isInFullscreen) {
        [self.playerController exitFullScreen];
    }
    
    [self.playerController stop];
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

// ---------------------------------------------------------------------------------

/**
 
 - (void)__panGestureAction:(UIPanGestureRecognizer *)sender {
 CGPoint translation = [sender translationInView:self.view];
 
 if (UIGestureRecognizerStateBegan == sender.state) {
 [self.playerController exitFullScreen];
 
 self.originalPosition = self.view.center;
 self.currentPosition = translation;
 }
 else if (UIGestureRecognizerStateChanged == sender.state) {
 CGRect viewRect = self.view.frame;
 
 viewRect.origin.y = translation.y;
 
 self.view.frame = viewRect;
 }
 else if (UIGestureRecognizerStateEnded == sender.state) {
 CGPoint velocity = [sender velocityInView:self.view];
 
 if (velocity.y >= 1500) {
 [UIView animateWithDuration:.25
 animations:^{
 CGRect viewRect = self.view.frame;
 
 viewRect.origin.y = CGRectGetHeight(viewRect);
 
 self.view.frame = viewRect;
 } completion:^(BOOL finished) {
 if (finished) {
 [self __dismissAction:nil];
 }
 }];
 }
 else {
 [UIView animateWithDuration:.25
 animations:^{
 self.view.center = self.originalPosition;
 }];
 }
 }
 }
 
 */

// ---------------------------------------------------------------------------------

@end

