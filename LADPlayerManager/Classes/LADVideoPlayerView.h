#import <UIKit/UIKit.h>

// External
#import <JWPlayer_iOS_SDK/JWPlayerController.h>

@class  JWConfig,
        LADVideoPlayerAnalyticsConfiguration;

// ---------------------------------------------------------------------------------

@interface LADVideoPlayerView : UIView

@property (nonatomic, assign) BOOL                      isFullScreen;
@property (nonatomic, strong) JWPlayerController        *playerController;

// ---------------------------------------------------------------------------------

+ (instancetype)viewWithConfiguration:(JWConfig *)configuration
               analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration;

// ---------------------------------------------------------------------------------

- (void)resize;

// ---------------------------------------------------------------------------------

- (void)stop;

// ---------------------------------------------------------------------------------

@end
