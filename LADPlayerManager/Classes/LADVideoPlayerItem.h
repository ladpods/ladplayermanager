#import <Foundation/Foundation.h>


@interface LADVideoPlayerItem : NSObject


+ (instancetype)itemWithName:(NSString *)name
                 imageSource:(NSString *)imageSource
                 videoSource:(NSString *)videoSource
                       adTag:(NSString *)adTag
                    playlist:(NSArray <LADVideoPlayerItem *>*)playlist;

/**
 *  The video name.
 */

@property (nonatomic, copy, readonly) NSString *name;

/**
 *  The video image source.
 */

@property (nonatomic, copy, readonly) NSString *imageSource;

/**
 *  The video source.
 */

@property (nonatomic, copy, readonly) NSString *videoSource;

/**
 *  The ad tag source.
 */

@property (nonatomic, copy, readonly) NSString *adTag;

/**
 *  The array of LADVideoPlayerItem objects that represents a playlist attached.
 */

@property (nonatomic, copy, readonly) NSArray <LADVideoPlayerItem *>*playlist;


@end
