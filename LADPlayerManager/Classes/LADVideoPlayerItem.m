#import "LADVideoPlayerItem.h"

// ---------------------------------------------------------------------------------

@implementation LADVideoPlayerItem

// ---------------------------------------------------------------------------------

+ (instancetype)itemWithName:(NSString *)name
                 imageSource:(NSString *)imageSource
                 videoSource:(NSString *)videoSource
                       adTag:(NSString *)adTag
                    playlist:(NSArray <LADVideoPlayerItem *>*)playlist {
    
    return [[self alloc] initWithName:name
                          imageSource:imageSource
                          videoSource:videoSource
                                adTag:adTag
                             playlist:playlist];
}

// ---------------------------------------------------------------------------------

- (instancetype)initWithName:(NSString *)name
                 imageSource:(NSString *)imageSource
                 videoSource:(NSString *)videoSource
                       adTag:(NSString *)adTag
                    playlist:(NSArray <LADVideoPlayerItem *>*)playlist{
    
    self = [super init];
    if (self) {
        _name= name;
        _imageSource = imageSource;
        _videoSource = videoSource;
        _adTag = adTag;
        _playlist = playlist;
    }
    
    return self;
}

// ---------------------------------------------------------------------------------

@end
