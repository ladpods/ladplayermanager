#import <Foundation/Foundation.h>


#import <JWPlayer_iOS_SDK/JWPlayerController.h>

// ---------------------------------------------------------------------------------

@interface LADVideoPlayerAnalyticsConfiguration : NSObject

@property (nonatomic, copy) void(^onPlay)(JWPlayerController * playerController);
@property (nonatomic, copy) void(^onPause)(JWPlayerController * playerController);
@property (nonatomic, copy) void(^onStop)(JWPlayerController * playerController);
@property (nonatomic, copy) void(^onStartSeeking)(JWPlayerController * playerController);
@property (nonatomic, copy) void(^onStopSeeking)(JWPlayerController * playerController);
@property (nonatomic, copy) void(^onStartChromecast)(JWPlayerController * playerController);
@property (nonatomic, copy) void(^onStartAirplay)(JWPlayerController * playerController);
@property (nonatomic, copy) void(^onStartAd)(JWPlayerController * playerController);

// ---------------------------------------------------------------------------------

@end
