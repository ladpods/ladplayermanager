#import <Foundation/Foundation.h>


@interface NSBundle (LADPlayerManagerAdditions)


+ (NSBundle *)lad_currentBundleForClass:(Class)aClass;


@end
