#import "NSBundle+LADPlayerManagerAdditions.h"


@implementation NSBundle (LADPlayerManagerAdditions)


+ (NSBundle *)lad_currentBundleForClass:(Class)aClass {
    NSBundle *podBundle = [NSBundle bundleForClass:aClass];
    NSURL *bundleURL = [podBundle URLForResource:@"LADPlayerManager"
                                   withExtension:@"bundle"];
    
    return [NSBundle bundleWithURL:bundleURL];
}


@end
