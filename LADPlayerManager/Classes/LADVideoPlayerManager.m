#import "LADVideoPlayerManager.h"

// Category
#import "JWConfig+LADAdditions.h"

// Controller
#import "LADVideoPlayerViewController.h"

// View
#import "LADVideoPlayerView.h"

// ---------------------------------------------------------------------------------

@implementation LADVideoPlayerManager

// ---------------------------------------------------------------------------------
#pragma mark - FullScreen

+ (void)fullScreenVideoPlayerWithItem:(LADVideoPlayerItem *)item
                        configuration:(LADVideoPlayerConfiguration *)configuration
               analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration
                           completion:(FullScreenCompletion)completion {
    
    if (!item) {
        return;
    }
    
    completion([LADVideoPlayerViewController viewControllerWithConfiguration:[JWConfig lad_configWithItem:item playerConfiguration:configuration]
                                                      analyticsConfiguration:analyticsConfiguration]);
}

// ---------------------------------------------------------------------------------
#pragma mark - Thumbnail

+ (void)thumbnailVideoPlayerWithItem:(LADVideoPlayerItem *)item
                       configuration:(LADVideoPlayerConfiguration *)configuration
              analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration
                          completion:(ThumnailCompletion)completion {
    
    if (!item) {
        return;
    }
    
    completion([LADVideoPlayerView viewWithConfiguration:[JWConfig lad_configWithItem:item playerConfiguration:configuration]
                                  analyticsConfiguration:analyticsConfiguration]);
}

// ---------------------------------------------------------------------------------

@end
