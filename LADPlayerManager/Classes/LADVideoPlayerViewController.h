#import <UIKit/UIKit.h>



@class  JWConfig,
        LADVideoPlayerAnalyticsConfiguration;


@interface LADVideoPlayerViewController : UIViewController

@property (nonatomic, assign)   BOOL                            allowRotation;
@property (nonatomic, assign)   UIInterfaceOrientationMask      presentationOrientation;

// ---------------------------------------------------------------------------------

+ (instancetype)viewControllerWithConfiguration:(JWConfig *)configuration
                         analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration;

// ---------------------------------------------------------------------------------

@end
