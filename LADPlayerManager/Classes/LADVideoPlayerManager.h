#import <Foundation/Foundation.h>

// ---------------------------------------------------------------------------------

@class  LADVideoPlayerItem,
        LADVideoPlayerConfiguration,
        LADVideoPlayerAnalyticsConfiguration,
        LADVideoPlayerViewController;

// ---------------------------------------------------------------------------------

typedef void(^FullScreenCompletion)(LADVideoPlayerViewController *videoPlayerViewController);
typedef void(^ThumnailCompletion)(UIView *videoPlayerView);

// ---------------------------------------------------------------------------------

@interface LADVideoPlayerManager : NSObject

// ---------------------------------------------------------------------------------

/**
 Get the full screen video
 
 @param item                   The video player item object.
 @param configuration          The configuration object.
 @param analyticsConfiguration The analytics configuration object.
 @param completion             The completion block returns the full screen video view controller.
 */

+ (void)fullScreenVideoPlayerWithItem:(LADVideoPlayerItem *)item
                        configuration:(LADVideoPlayerConfiguration *)configuration
               analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration
                           completion:(FullScreenCompletion)completion;


// ---------------------------------------------------------------------------------
/**
 Get the thumbnail screen video
 
 @param item                   The video player item object.
 @param configuration          The configuration object.
 @param analyticsConfiguration The analytics configuration object.
 @param completion             The completion block returns the thumbnail screen video view.
 */

+ (void)thumbnailVideoPlayerWithItem:(LADVideoPlayerItem *)item
                       configuration:(LADVideoPlayerConfiguration *)configuration
              analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration
                          completion:(ThumnailCompletion)completion;

// ---------------------------------------------------------------------------------

@end
