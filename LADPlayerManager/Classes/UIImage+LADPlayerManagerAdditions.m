#import "UIImage+LADPlayerManagerAdditions.h"

// Category
#import "NSBundle+LADPlayerManagerAdditions.h"


@implementation UIImage (LADPlayerManagerAdditions)


+ (UIImage *)lad_imageNamed:(NSString *)name
                    inClass:(Class)aClass {
    
    return [UIImage imageNamed:name
                      inBundle:[NSBundle lad_currentBundleForClass:aClass]
 compatibleWithTraitCollection:nil];
}


@end
