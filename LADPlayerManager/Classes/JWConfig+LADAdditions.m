#import "JWConfig+LADAdditions.h"

// Configuration
#import "LADVideoPlayerConfiguration.h"

// External
#import <GoogleInteractiveMediaAds/GoogleInteractiveMediaAds.h>

// Model
#import "LADVideoPlayerItem.h"

// ---------------------------------------------------------------------------------

@implementation JWConfig (LADAdditions)

// ---------------------------------------------------------------------------------

+ (JWConfig *)lad_configWithItem:(LADVideoPlayerItem *)item
             playerConfiguration:(LADVideoPlayerConfiguration *)playerConfiguration{
    
    JWConfig *config = [[JWConfig alloc] initWithContentUrl:item.videoSource];
    config.title = item.name;
    config.image = item.imageSource;
    config.size = playerConfiguration.size;
    config.cssSkin = playerConfiguration.cssURLString.length ? playerConfiguration.cssURLString : @"https://ota.ladmedia.fr/mobiles/default_fullscreen.css?a";
    config.autostart = YES;
    
    // Ads config
    if (item.adTag &&
        item.adTag.length) {
        
        config.adSchedule = @[[JWAdBreak adBreakWithTag:item.adTag
                                                 offset:nil]];
        
        JWAdConfig *adConfig = [JWAdConfig new];
        adConfig.adClient = googIMA;
        adConfig.skipText = playerConfiguration.adSkipText.length ? playerConfiguration.adSkipText : @"Passer";
        IMASettings *settings = [[IMASettings alloc] init];
        settings.ppid = @"IMA_PPID_0";
        settings.language = @"fr";
        adConfig.imaSettings = settings;
        
        config.adConfig = adConfig;
    }
    
    // Playlist
    if (item.playlist &&
        item.playlist.count) {
        
        NSMutableArray *playlist = [[NSMutableArray alloc] initWithCapacity:item.playlist.count];
        for (LADVideoPlayerItem *playerItem in item.playlist) {
            JWPlaylistItem *playlistItem = [JWPlaylistItem new];
            playlistItem.file = playerItem.videoSource;
            playlistItem.title = playerItem.name;
            playlistItem.image = playerItem.imageSource;
            
            if (playerItem.adTag &&
                playerItem.adTag.length) {
                
                playlistItem.adSchedule = @[[JWAdBreak adBreakWithTag:playerItem.adTag
                                                               offset:nil]];
            }
            
            [playlist addObject:playlistItem];
        }
        
        config.playlist = playlist;
    }
    
    return config;
}

// ---------------------------------------------------------------------------------

@end
