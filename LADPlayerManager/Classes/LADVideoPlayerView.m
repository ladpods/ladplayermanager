#import "LADVideoPlayerView.h"

// Category
#import "NSBundle+LADPlayerManagerAdditions.h"

// Configurations
#import "LADVideoPlayerAnalyticsConfiguration.h"

// External
#import <JWPlayer_iOS_SDK/JWPlayerController.h>

// Model
#import "LADVideoPlayerItem.h"

// View
#import "LADVideoPlayerControlsView.h"

// ---------------------------------------------------------------------------------
#pragma mark - Interface

@interface LADVideoPlayerView (/* Private */) <JWPlayerDelegate>

@property (nonatomic, strong) LADVideoPlayerControlsView            *controlsView;
@property (nonatomic, strong) JWConfig                              *configuration;
@property (nonatomic, strong) LADVideoPlayerAnalyticsConfiguration  *analyticsConfiguration;
@property (nonatomic, assign) BOOL                                  barOnScreen;
@property (nonatomic, assign) BOOL                                  isInPictureInPicure;

// Action
- (void)__dismiss;

// Utils
- (void)__handleControlsView;

@end

// ---------------------------------------------------------------------------------
#pragma mark - Implementation


@implementation LADVideoPlayerView

// ---------------------------------------------------------------------------------
#pragma mark - Intitialization

+ (instancetype)viewWithConfiguration:(JWConfig *)configuration
               analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration {
    
    return [[self alloc] initWithConfiguration:configuration
                        analyticsConfiguration:analyticsConfiguration];
}

// ---------------------------------------------------------------------------------

- (instancetype)initWithConfiguration:(JWConfig *)configuration
               analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration {
    
    self = [super init];
    
    if (self) {
        _configuration = configuration;
        _analyticsConfiguration = analyticsConfiguration;
    }
    
    return self;
}

// ---------------------------------------------------------------------------------
#pragma mark - Properties

- (JWPlayerController *)playerController {
    
    if (!_playerController) {
        _playerController = [[JWPlayerController alloc] initWithConfig:self.configuration
                                                              delegate:self];
        _playerController.forceFullScreenOnLandscape = YES;
        [self addSubview:_playerController.view];
    }
    
    return _playerController;
}

// ---------------------------------------------------------------------------------

- (void)stop
{
    if (_playerController) {
        
        [_playerController stop];
        
        if (self.analyticsConfiguration &&
            self.analyticsConfiguration.onStop) {
            
            self.analyticsConfiguration.onStop(self.playerController);
        }
    }
}

// ---------------------------------------------------------------------------------

- (LADVideoPlayerControlsView *)controlsView {
    
    if (!_controlsView) {
        
        _controlsView = [[NSBundle lad_currentBundleForClass:[LADVideoPlayerControlsView class]] loadNibNamed:NSStringFromClass([LADVideoPlayerControlsView class]) owner:self options:nil][0];
        _controlsView.backgroundColor = [UIColor clearColor];
        _controlsView.playerController = self.playerController;
        _controlsView.hidden = YES;
        
        __weak typeof(self) weakSelf = self;
        
        _controlsView.onCloseAction = ^{
            
            typeof(self) strongSelf = weakSelf;
            
            [strongSelf.playerController stop];
            [strongSelf __dismiss];
        };
        
        [self.playerController.view addSubview:_controlsView];
    }
    
    return _controlsView;
}

// ---------------------------------------------------------------------------------
#pragma mark - Layout

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGRect rect = self.playerController.view.frame;
    rect.origin.x = 0;
    rect.origin.y = 0;
    
    self.playerController.view.frame = rect;
    
    self.controlsView.frame = CGRectMake(0,
                                         0,
                                         self.controlsView.currentWidth,
                                         44);
}

// ---------------------------------------------------------------------------------

- (void)resize
{
    NSLog(@"\n\n I am in resize 1 \n\n");
    
    if(self.isFullScreen && !self.isInPictureInPicure)
    {
        NSLog(@"\n\n I am in resize 2 \n\n");
        
        for (UIWindow * window in [UIApplication sharedApplication].windows)
        {
            window.frame = [UIApplication sharedApplication].delegate.window.frame;
            self.playerController.view.frame = window.frame;
            
            NSLog(@"\n\n I am in resize 3 \n\n");
            
            break;
        }
        
    }else{
        UIApplication.sharedApplication.statusBarHidden = false;
    }
}

// ---------------------------------------------------------------------------------
#pragma mark - JWPlayerDelegate

- (void)onPlay:(NSString *)oldState {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onPlay) {
        
        self.analyticsConfiguration.onPlay(self.playerController);
    }
    
    [self.playerController enterFullScreen];
    self.barOnScreen = YES;
    [self __handleControlsView];
}

// ---------------------------------------------------------------------------------

- (void)onPause:(NSString *)oldState {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onPause) {
        
        self.analyticsConfiguration.onPause(self.playerController);
    }
}

// ---------------------------------------------------------------------------------

- (void)onComplete {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onStop) {
        
        self.analyticsConfiguration.onStop(self.playerController);
    }
    
    [self __dismiss];
}

// ---------------------------------------------------------------------------------

- (void)onSeek:(double)offset fromPosition:(double)position {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onStartSeeking) {
        
        self.analyticsConfiguration.onStartSeeking(self.playerController);
    }
}

// ---------------------------------------------------------------------------------

- (void)onSeeked {
    
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onStopSeeking) {
        
        self.analyticsConfiguration.onStopSeeking(self.playerController);
    }
}

// ---------------------------------------------------------------------------------

- (void)onFullscreen:(BOOL)status {
    
    if (!status) {
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait)
                                    forKey:@"orientation"];
    }
    
    self.isFullScreen = status;
    
    [self resize];
    
    [self __handleControlsView];
}

// ---------------------------------------------------------------------------------

- (void)onControlBarVisible:(BOOL)isVisible {
    
    self.barOnScreen = isVisible;
    
    [self __handleControlsView];
}

// ---------------------------------------------------------------------------------

- (void)onPictureInPicture:(BOOL)status{
    
    self.isInPictureInPicure = status;
    
    if(!status){
        [self resize];
    }
}

// ---------------------------------------------------------------------------------

- (void)onError:(NSError *)error{
    
    NSLog(@"LADVideoPlayerView : Error => %@",[error description]);
}

// ---------------------------------------------------------------------------------

- (void)onAdError:(NSError *)error{
    
    NSLog(@"LADVideoPlayerView : Ad Error => %@",[error description]);
    
    /// PATCH : video content not play on adError if tag is configure
    /// with an empty configuration
    
    // if we force a pause that relaunch content play, why don't know
    // may be a bug in jwplayer sdk
    [self.playerController pause];
}


// ---------------------------------------------------------------------------------
#pragma mark - Action 

- (void)__dismiss {
    
    // stop tracking / rich media function
    if (self.analyticsConfiguration &&
        self.analyticsConfiguration.onStop) {
        
        self.analyticsConfiguration.onStop(self.playerController);
    }
    
    [self.playerController exitFullScreen];
    [self removeFromSuperview];
}

// ---------------------------------------------------------------------------------
#pragma mark - Utils


- (void)__handleControlsView {
    if (self.playerController.isInFullscreen) {
        self.controlsView.hidden = !self.barOnScreen;
    }
    else {
        self.controlsView.hidden = YES;
    }
}

// ---------------------------------------------------------------------------------


@end
