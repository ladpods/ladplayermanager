#import "LADVideoPlayerControlsView.h"

// Category
#import "UIImage+LADPlayerManagerAdditions.h"

// External
#import <JWPlayer_iOS_SDK/JWPlayerController.h>
#import <GoogleCast/GoogleCast.h>
#import <MediaPlayer/MediaPlayer.h>

// ---------------------------------------------------------------------------------

typedef NS_ENUM(NSUInteger, ChromecastButtonType) {
    ChromecastAnimatingButtonType = 0,
    ChromecastConnectedButtonType,
    ChromecastDisconnectedButtonType,
    ChromecastStartCastingButtonType,
    ChromecastEndCastingButtonType
};

// ---------------------------------------------------------------------------------
#pragma mark - Interface    

@interface LADVideoPlayerControlsView () <JWCastingDelegate>

// ---------------------------------------------------------------------------------

@property (nonatomic, weak) IBOutlet UIButton *closeButton;
// Chromecast
@property (nonatomic, strong) JWCastController *chromecastController;
@property (nonatomic, strong) NSArray *chromecastDevices;
@property (nonatomic, strong) UIButton *chromecastButton;
// Airplay
@property (nonatomic, strong) MPVolumeView *airplayView;

@property (nonatomic, assign) CGFloat currentWidth;

// ---------------------------------------------------------------------------------

// Configure
- (void)__configureCast;
- (void)__configureChromecast;
- (void)__configureAirplay;
- (void)__configureChromecastButtonWithType:(ChromecastButtonType)type;

// Action
- (IBAction)__closeAction:(id)sender;
- (void)__chromecastAction;


@end

// ---------------------------------------------------------------------------------
#pragma mark - Implementation


@implementation LADVideoPlayerControlsView

// ---------------------------------------------------------------------------------
#pragma mark - Property

- (CGFloat)currentWidth {
    
    CGFloat maxX = CGRectGetMaxX(self.closeButton.frame) ;
    
    if (self.chromecastButton) {
        maxX = CGRectGetMaxX(self.chromecastButton.frame) ;
    }
    
    if (self.airplayView) {
        maxX = CGRectGetMaxX(self.airplayView.frame);
    }
    
    return maxX;
}

// ---------------------------------------------------------------------------------

- (void)setPlayerController:(JWPlayerController *)playerController {
    
    _playerController = playerController;
    
    [self __configureCast];
}

// ---------------------------------------------------------------------------------
#pragma mark - Layout

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGFloat maxX = CGRectGetMaxX(self.closeButton.frame);
    
    if (self.chromecastButton) {
        self.chromecastButton.frame = CGRectMake(maxX,
                                                 CGRectGetHeight(self.bounds) / 2 - 44/2,
                                                 44,
                                                 44);
        
        maxX = CGRectGetMaxX(self.chromecastButton.frame);
    }
    
    self.airplayView.frame = CGRectMake(maxX,
                                        CGRectGetHeight(self.bounds) / 2 - 44/2,
                                        44,
                                        44);
}

// ---------------------------------------------------------------------------------
#pragma mark - JWCastingDelegate

- (void)onCastingDevicesAvailable:(NSArray<JWCastingDevice *> *)devices {
    
    self.chromecastDevices = devices;
    
    if (devices.count &&
        !self.chromecastButton) {
        
        self.chromecastButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.chromecastButton.imageView.animationImages = @[[[UIImage lad_imageNamed:@"cast_connecting0" inClass:self.class] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate],
                                                            [[UIImage lad_imageNamed:@"cast_connecting1" inClass:self.class] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate],
                                                            [[UIImage lad_imageNamed:@"cast_connecting2" inClass:self.class] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate],
                                                            [[UIImage lad_imageNamed:@"cast_connecting1" inClass:self.class] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        self.chromecastButton.imageView.animationDuration = 2;
        [self.chromecastButton addTarget:self
                                  action:@selector(__chromecastAction)
                        forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.chromecastButton];
        
        [self __configureChromecastButtonWithType:ChromecastDisconnectedButtonType];
    }
}

// ---------------------------------------------------------------------------------

- (void)onConnectedToCastingDevice:(JWCastingDevice *)device {
    
    [self __configureChromecastButtonWithType:ChromecastConnectedButtonType];
    [self.chromecastController cast];
}

// ---------------------------------------------------------------------------------

- (void)onDisconnectedFromCastingDevice:(NSError *)error {
    [self __configureChromecastButtonWithType:ChromecastDisconnectedButtonType];
}

// ---------------------------------------------------------------------------------

- (void)onConnectionTemporarilySuspended {
    [self __configureChromecastButtonWithType:ChromecastAnimatingButtonType];
}

// ---------------------------------------------------------------------------------

- (void)onConnectionRecovered {
    [self __configureChromecastButtonWithType:ChromecastConnectedButtonType];
}

// ---------------------------------------------------------------------------------

- (void)onConnectionFailed:(NSError *)error {
    [self __configureChromecastButtonWithType:ChromecastDisconnectedButtonType];
}

// ---------------------------------------------------------------------------------

- (void)onCasting {
    [self __configureChromecastButtonWithType:ChromecastStartCastingButtonType];
}

// ---------------------------------------------------------------------------------

-(void)onCastingEnded:(NSError *)error {
    [self __configureChromecastButtonWithType:ChromecastEndCastingButtonType];
}

// ---------------------------------------------------------------------------------

-(void)onCastingFailed:(NSError *)error {
    [self __configureChromecastButtonWithType:ChromecastEndCastingButtonType];
}

// ---------------------------------------------------------------------------------
#pragma mark - Configure

- (void)__configureCast {
    [self __configureChromecast];
    [self __configureAirplay];
}

// ---------------------------------------------------------------------------------

- (void)__configureChromecast {
    
    self.chromecastController = [[JWCastController alloc] initWithPlayer:self.playerController];
    self.chromecastController.delegate = self;
    self.chromecastController.chromeCastReceiverAppID = kGCKMediaDefaultReceiverApplicationID;
    
    [self.chromecastController scanForDevices];
}

// ---------------------------------------------------------------------------------

- (void)__configureAirplay{
    
    self.airplayView = [MPVolumeView new];
    self.airplayView.backgroundColor = [UIColor clearColor];
    self.airplayView.showsVolumeSlider = NO;
    
    [self addSubview:self.airplayView];
}

// ---------------------------------------------------------------------------------

- (void)__configureChromecastButtonWithType:(ChromecastButtonType)type {
    
    if (ChromecastAnimatingButtonType == type) {
        self.chromecastButton.tintColor = [UIColor whiteColor];
        [self.chromecastButton.imageView startAnimating];
    }
    else if (ChromecastConnectedButtonType == type) {
        [self.chromecastButton.imageView stopAnimating];
        [self.chromecastButton setImage:[[UIImage lad_imageNamed:@"cast_on" inClass:self.class] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                               forState:UIControlStateNormal];
        self.chromecastButton.tintColor = [UIColor colorWithRed:0.10 green:0.84 blue:0.99 alpha:1.0];
    }
    else if (ChromecastDisconnectedButtonType == type) {
        [self.chromecastButton.imageView stopAnimating];
        [self.chromecastButton setImage:[[UIImage lad_imageNamed:@"cast_off" inClass:self.class] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                               forState:UIControlStateNormal];
        self.chromecastButton.tintColor = [UIColor whiteColor];
    }
    else if (ChromecastStartCastingButtonType == type) {
        self.chromecastButton.tintColor = [UIColor colorWithRed:0.53 green:0.99 blue:0.44 alpha:1.0];
    }
    else if (ChromecastEndCastingButtonType == type) {
        self.chromecastButton.tintColor = [UIColor colorWithRed:0.10 green:0.84 blue:0.99 alpha:1.0];
    }
}

// ---------------------------------------------------------------------------------
#pragma mark - Action 

- (IBAction)__closeAction:(id)sender {
    
    if (self.onCloseAction) {
        self.onCloseAction();
    }
}

// ---------------------------------------------------------------------------------

- (void)__chromecastAction {
    
    __weak __typeof__(self) weakSelf = self;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    if (!self.chromecastController.connectedDevice) {
        alertController.title = @"Connexion à";
        
        [self.chromecastController.availableDevices enumerateObjectsUsingBlock:^(JWCastingDevice  *_Nonnull device, NSUInteger idx, BOOL * _Nonnull stop) {
            UIAlertAction *deviceSelected = [UIAlertAction actionWithTitle:device.name
                                                                     style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                                       [weakSelf.chromecastController connectToDevice:device];
                                                                       [weakSelf __configureChromecastButtonWithType:ChromecastAnimatingButtonType];
                                                                   }];
            [alertController addAction:deviceSelected];
        }];
    } else {
        alertController.title = self.chromecastController.connectedDevice.name;
        alertController.message = @"Sélectionner une action";
        
        UIAlertAction *disconnect = [UIAlertAction actionWithTitle:@"Déconnecter"
                                                             style:UIAlertActionStyleDestructive
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               [weakSelf.chromecastController disconnect];
                                                           }];
        [alertController addAction:disconnect];
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Annuler"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    [alertController addAction:cancel];
    
    [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alertController
                                                                                   animated:YES
                                                                                 completion:nil];
}

// ---------------------------------------------------------------------------------

@end
