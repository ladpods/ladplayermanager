#import <UIKit/UIKit.h>


@class JWPlayerController;

// ---------------------------------------------------------------------------------

@interface LADVideoPlayerControlsView : UIView

@property (nonatomic, strong) JWPlayerController *playerController;

@property (nonatomic, copy) void(^onCloseAction)(void);
@property (nonatomic, copy) void(^onStartChromecastAction)(void);
@property (nonatomic, copy) void(^onStartAirplayAction)(void);

@property (nonatomic, assign, readonly) CGFloat currentWidth;


@end
