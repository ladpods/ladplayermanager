#import <Foundation/Foundation.h>


@interface LADVideo : NSObject


+ (instancetype)videoWithName:(NSString *)name
                    URLString:(NSString *)URLString;

@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSURL *URL;


@end
