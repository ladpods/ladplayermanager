#import "LADVideoCell.h"

// Player
#import <LADPlayerManager/LADVideoPlayer.h>
#import <LADPlayerManager/LADVideoPlayerViewController.h>

// ---------------------------------------------------------------------------------
#pragma mark - Interface

@interface LADVideoCell ()

@property (nonatomic, strong) LADVideoPlayerItem *item;
@property (nonatomic, weak) IBOutlet UIView *videoPlayerContainer;

@end

// ---------------------------------------------------------------------------------

@implementation LADVideoCell

// ---------------------------------------------------------------------------------

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    for (UIView *subView in self.videoPlayerContainer.subviews) {
        [subView removeFromSuperview];
    }
}

// ---------------------------------------------------------------------------------

- (void)configureWithItem:(LADVideoPlayerItem *)item
{
    self.item = item;
    self.titleLabel.text = item.name;
}

// ---------------------------------------------------------------------------------

+ (NSString *)identifier
{
    return @"VideoCellIdentifier";
}

// ---------------------------------------------------------------------------------

- (IBAction)playVideoAction:(id)sender
{
    LADVideoPlayerConfiguration *configuration = [LADVideoPlayerConfiguration new];
    configuration.size = CGSizeMake(CGRectGetWidth(self.videoPlayerContainer.bounds), CGRectGetHeight(self.videoPlayerContainer.bounds));
    
    ///**
    
    // Fullscreen
    [LADVideoPlayerManager fullScreenVideoPlayerWithItem:self.item
                                           configuration:configuration
                                  analyticsConfiguration:nil
                                              completion:^(LADVideoPlayerViewController *videoPlayerViewController) {
                                                  
                                                  // Disable roation => shouldAutorotate
                                                  //[videoPlayerViewController setAllowRotation:FALSE];
                                                  
                                                  // Define presentation orientation
                                                  //[videoPlayerViewController setPresentationOrientation:UIInterfaceOrientationMaskLandscape];
                                                  
                                                  [self.window.rootViewController presentViewController:videoPlayerViewController
                                                                                               animated:YES
                                                                                             completion:nil];
                                              }];
     
     //*/
    
    /**
    
    // Thumbnail
    [LADVideoPlayerManager thumbnailVideoPlayerWithItem:self.item
                                          configuration:configuration
                                 analyticsConfiguration:nil
                                             completion:^(UIView *videoPlayerView) {
                                                 videoPlayerView.frame = self.videoPlayerContainer.bounds;
                                                 [self.videoPlayerContainer addSubview:videoPlayerView];
                                             }];
     
     */
}

// ---------------------------------------------------------------------------------

@end
