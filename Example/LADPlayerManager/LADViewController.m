#import "LADViewController.h"

// Model
#import "LADVideo.h"

// Player
#import <LADPlayerManager/LADVideoPlayer.h>

// View
#import "LADVideoCell.h"

// ---------------------------------------------------------------------------------
#pragma mark - Interface

@interface LADViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate>

@property (nonatomic, weak) IBOutlet    UICollectionView                *collectionView;
@property (nonatomic, weak) IBOutlet    UICollectionViewFlowLayout      *flowLayout;
@property (nonatomic, strong)           NSArray                         *videos;
@property (nonatomic, assign)           BOOL                            playing;

@end

// ---------------------------------------------------------------------------------
#pragma mark - Implementation

@implementation LADViewController

// ---------------------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.flowLayout.itemSize = CGSizeMake(CGRectGetWidth(self.view.bounds), CGRectGetWidth(self.view.bounds));
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithURL:[NSURL URLWithString:@"http://cdn2-api-content-elle.ladmedia.fr/files/json/elle_api_alaune_mobile_ios_videos.json"]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if (!error) {
                    // Success
                    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                        NSError *jsonError;
                        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                        
                        if (!jsonError) {
                            NSMutableArray *videosArray = [NSMutableArray new];
                            
                            for (NSDictionary *video in jsonResponse[@"r"]) {
                                
                                NSDictionary *videoFormat = video[@"format"];
                                NSString *videoAdTag = @"";
                                
                                // ad tag from EAT
                                videoAdTag = @"https://pubads.g.doubleclick.net/gampad/ads?sz=1920x1080&iu=/12271007/dfp_app_smartphones_elle_a_table_s137074/preroll_videos_r178749/f178751_d321video_pre_roll_1_preroll_videos_16_9_1920x1080&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]";
                                
                                // =====================
                                // Google ad tag sample
                                // =====================
                                
                                // Single Inline Linear
                                //videoAdTag = @"https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=";
                                
                                // Single Skippable Inline
                                //videoAdTag = @"https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator=";
                                
                                // Single Redirect Error
                                videoAdTag = @"https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dredirecterror&nofb=1&correlator=";
                                
                                // VMAP Pre-, Mid-, and Post-rolls, Single Ads
                                //videoAdTag = @"https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpost&cmsid=496&vid=short_onecue&correlator=";
                                
                                //
                                //videoAdTag = @"";
                                
                                
                                LADVideoPlayerItem *item = [LADVideoPlayerItem itemWithName:@""
                                                                                imageSource:nil
                                                                                videoSource:videoFormat[@"hd"]
                                                                                      adTag:videoAdTag
                                                                                   playlist:nil];
                                
                                [videosArray addObject:item];
                            }
                            
                            self.videos = [videosArray mutableCopy];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.collectionView reloadData];
                            });
                        }
                    }
                }
            }] resume];
}

// ---------------------------------------------------------------------------------

- (BOOL)shouldAutorotate
{
    return self.playing;
}

// ---------------------------------------------------------------------------------

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

// ---------------------------------------------------------------------------------
#pragma mark  - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

// ---------------------------------------------------------------------------------

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.videos.count;
}

// ---------------------------------------------------------------------------------

- (LADVideoCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LADVideoCell *cell = (LADVideoCell *)[collectionView  dequeueReusableCellWithReuseIdentifier:[LADVideoCell identifier]
                                                                                    forIndexPath:indexPath];
    
    cell.layer.borderWidth = 2.f;
    
    LADVideoPlayerItem *item = self.videos[indexPath.row];
    self.playing = YES;
    
    [cell configureWithItem:item];
    
    return cell;
}

// ---------------------------------------------------------------------------------
#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    LADVideo *video = self.videos[indexPath.row];
//    NSString *selectedVideoAbsoluteString = video.URL.absoluteString;
//    NSMutableArray *playlist = [NSMutableArray new];
//    
//    for (LADVideo *video in self.videos) {
//        NSString *videoURLAbsoluteString = video.URL.absoluteString;
//        
//        if (![videoURLAbsoluteString isEqualToString:selectedVideoAbsoluteString]) {
//            [playlist addObject:[LADVideoPlayerItem itemWithName:video.name imageSource:nil videoSource:videoURLAbsoluteString adTag:nil playlist:nil]];
//        }
//    }
//    
//    LADVideoPlayerItem *item = [LADVideoPlayerItem itemWithName:video.name
//                                                    imageSource:nil
//                                                    videoSource:selectedVideoAbsoluteString
//                                                          adTag:nil
//                                                       playlist:playlist];
}

// ---------------------------------------------------------------------------------

@end
