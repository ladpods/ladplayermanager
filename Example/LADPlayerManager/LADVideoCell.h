#import <UIKit/UIKit.h>


@class LADVideoPlayerItem;


@interface LADVideoCell : UICollectionViewCell


@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
- (IBAction)playVideoAction:(id)sender;

- (void)configureWithItem:(LADVideoPlayerItem *)item;
+ (NSString *)identifier;


@end
