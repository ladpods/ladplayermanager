//
//  LADAppDelegate.h
//  LADPlayerManager
//
//  Created by Jean-baptiste Castro on 02/01/2017.
//  Copyright (c) 2017 Jean-baptiste Castro. All rights reserved.
//

@import UIKit;

@interface LADAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
