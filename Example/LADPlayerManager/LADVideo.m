#import "LADVideo.h"


@implementation LADVideo


+ (instancetype)videoWithName:(NSString *)name
                    URLString:(NSString *)URLString {
    
    return [[self alloc] initWithName:name
                            URLString:URLString];
}


- (instancetype)initWithName:(NSString *)name
                   URLString:(NSString *)URLString{
    
    self = [super init];
    if (self) {
        _name = name;
        _URL = [NSURL URLWithString:URLString];
    }
    
    return self;
}


@end
