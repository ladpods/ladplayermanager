//
//  main.m
//  LADPlayerManager
//
//  Created by Jean-baptiste Castro on 02/01/2017.
//  Copyright (c) 2017 Jean-baptiste Castro. All rights reserved.
//

@import UIKit;
#import "LADAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LADAppDelegate class]));
    }
}
