#import <Foundation/Foundation.h>


@interface LADVideoPlayerConfiguration : NSObject


/**
 *  The size of the video player. Uses in thumbnail mode.
 */

@property (nonatomic, assign) CGSize size;

/**
 *  The css url link to customize video player appearance.
 */

@property (nonatomic, copy) NSString *cssURLString;

/**
 *  The message to show when skip is available. Default is "Passer".
 */

@property (nonatomic, copy) NSString *adSkipText;


@end
