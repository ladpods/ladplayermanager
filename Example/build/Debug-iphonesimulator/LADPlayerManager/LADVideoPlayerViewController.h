#import <UIKit/UIKit.h>


@class  JWConfig,
        LADVideoPlayerAnalyticsConfiguration;


@interface LADVideoPlayerViewController : UIViewController


+ (instancetype)viewControllerWithConfiguration:(JWConfig *)configuration
                         analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration;


@end
