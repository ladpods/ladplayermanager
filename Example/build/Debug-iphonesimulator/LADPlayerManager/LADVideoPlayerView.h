#import <UIKit/UIKit.h>


@class  JWConfig,
        LADVideoPlayerAnalyticsConfiguration;


@interface LADVideoPlayerView : UIView


+ (instancetype)viewWithConfiguration:(JWConfig *)configuration
               analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration;
+ (BOOL)exists;


@end
