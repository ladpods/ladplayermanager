#import <UIKit/UIKit.h>


@class JWPlayerController;


@interface LADVideoPlayerControlsView : UIView


@property (nonatomic, strong) JWPlayerController *playerController;

@property (nonatomic, copy) void(^onCloseAction)();
@property (nonatomic, copy) void(^onStartChromecastAction)();
@property (nonatomic, copy) void(^onStartAirplayAction)();

@property (nonatomic, assign, readonly) CGFloat currentWidth;


@end
