#import "JWConfig.h"

@class  LADVideoPlayerItem,
        LADVideoPlayerConfiguration;


@interface JWConfig (LADAdditions)


+ (JWConfig *)lad_configWithItem:(LADVideoPlayerItem *)item
             playerConfiguration:(LADVideoPlayerConfiguration *)playerConfiguration;


@end
