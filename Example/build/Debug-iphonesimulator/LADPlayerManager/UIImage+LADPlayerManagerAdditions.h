#import <UIKit/UIKit.h>


@interface UIImage (LADPlayerManagerAdditions)


+ (UIImage *)lad_imageNamed:(NSString *)name
                    inClass:(Class)aClass;


@end
