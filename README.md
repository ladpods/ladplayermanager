
# LADPlayerManager 

Video Player based on JWPlayer SDK:
- Play a video
- Add a playlist
- Cast a video
- Add ad tag
- ...

## Summary

- [Installation](#installation)
- [Usage](#usage)
- [LADVideoPlayerItem](#ladvideoplayeritem)
- [LADVideoPlayerConfiguration](#ladvideoplayerconfiguration)
- [LADVideoPlayerAnalyticsConfiguration](#ladvideoplayeranalyticsconfiguration)
- [Examples](#examples)
- [Author](#author)
- [License](#license)

## Installation

Add in **Podfile**:
```ruby
pod 'LADPlayerManager'
```

Add your JWPlayer key in **info.plist**:
![PLIST](https://photos-6.dropbox.com/t/2/AADWIhGHUT3Uz9CRkladeEJT8gGew5UCvA1xamP-xdylJA/12/552813713/png/32x32/1/_/1/2/LADPlayerManagerInstallation.png/ENq9648DGKQMIAIoAg/tFh7fdL-m-Cbc2oPJEguXUvsXn9Y19eAqyTTfoHc1lA?size=1600x1200&size_mode=3)

Import:
```objc
#import <LADPlayerManager/LADVideoPlayer.h>
```

## Usage
You can use **LADPlayerManager** in two different modes.
In **Fullscreen mode**:      
```objc     
+ (void)fullScreenVideoPlayerWithItem:(LADVideoPlayerItem *)item
                        configuration:(LADVideoPlayerConfiguration *)configuration
               analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration
                           completion:(FullScreenCompletion)completion;

```

>**item** : The [LADVideoPlayerItem](#ladvideoplayeritem) object.                      
>**configuration** : The [LADVideoPlayerConfiguration](#ladvideoplayerconfiguration) object.        
>**analyticsConfiguration** : The [LADVideoPlayerAnalyticsConfiguration](#ladvideoplayeranalyticsconfiguration) object.       
>**completion** : The completion block returns the full screen video view controller        
```objc
typedef void(^FullScreenCompletion)(UIViewController *videoPlayerViewController);   
```         

In **Thumbnail mode** :
```objc     
+ (void)thumbnailVideoPlayerWithItem:(LADVideoPlayerItem *)item
                       configuration:(LADVideoPlayerConfiguration *)configuration
              analyticsConfiguration:(LADVideoPlayerAnalyticsConfiguration *)analyticsConfiguration
                          completion:(ThumnailCompletion)completion;

```

>**item** : The [LADVideoPlayerItem](#ladvideoplayeritem) object.                      
>**configuration** : The [LADVideoPlayerConfiguration](#ladvideoplayerconfiguration) object.        
>**analyticsConfiguration** : The [LADVideoPlayerAnalyticsConfiguration](#ladvideoplayeranalyticsconfiguration) object.       
>**completion** : The completion block returns the full screen video view controller        
```objc
typedef void(^ThumnailCompletion)(UIView *videoPlayerView); 
``` 

## LADVideoPlayerItem

Represents the video player item. 
```objc   
/**
 *  The video name.
 */

@property (nonatomic, copy, readonly) NSString *name;

/**
 *  The video image source.
 */

@property (nonatomic, copy, readonly) NSString *imageSource;

/**
 *  The video source.
 */

@property (nonatomic, copy, readonly) NSString *videoSource;

/**
 *  The ad tag source.
 */

@property (nonatomic, copy, readonly) NSString *adTag;

/**
 *  The array of LADVideoPlayerItem objects that represents a playlist attached.
 */

@property (nonatomic, copy, readonly) NSArray <LADVideoPlayerItem *>*playlist;
```
## LADVideoPlayerConfiguration

Represents the video player configuration.
```objc

/**
 *  The size of the video player. Uses in thumbnail mode.
 */

@property (nonatomic, assign) CGSize size;

/**
 *  The css url link to customize video player appearance.
 */

@property (nonatomic, copy) NSString *cssURLString;

/**
 *  The message to show when skip is available. Default is "Passer".
 */

@property (nonatomic, copy) NSString *adSkipText;
```

## LADVideoPlayerAnalyticsConfiguration

Represents the video player analytics configuration.
```objc

/**
 *  Called when video start playing.
 */

@property (nonatomic, copy) void(^onPlay)();

/**
 *  Called when video is paused.
 */

@property (nonatomic, copy) void(^onPause)();

/**
 *  Called when video stops.
 */

@property (nonatomic, copy) void(^onStop)();

/**
 *  Called when the video starts seeking.
 */

@property (nonatomic, copy) void(^onStartSeeking)();

/**
 *  Called when the video stops seeking.
 */

@property (nonatomic, copy) void(^onStopSeeking)();
```



## Examples

- Create video item:

```objc
LADVideoPlayerItem *item = [LADVideoPlayerItem itemWithName:@"name" imageSource:@"image" videoSource:@"video" adTag:@"tag" playlist:@[item1, item2]];
```

- Create video configuration:

```objc
LADVideoPlayerConfiguration *configuration = [LADVideoPlayerConfiguration new];
configuration.size = CGSizeMa(100, 100);
configuration.castOption = LADVideoPlayerAllCastOption;
configuration.cssURLString = @"LinkToYourCSS";
configuration.adSkipText = @"SKIIIIP";
```

- Create analytics configuration:

```objc
LADVideoPlayerAnalyticsConfiguration *analyticsConfiguration = [LADVideoPlayerAnalyticsConfiguration new];
analyticsConfiguration.onPlay = ^{
    // Tag play
};
analyticsConfiguration.onStartSeeking = ^{
    // Tag start seeking
};
```

- Get fullscreen video player:

![FULLSCREEN](https://gitlab.com/ladpods/ladplayermanager/raw/d313c20f3479d2322add3d35bd82295ea92e09b6/IMG_0041.PNG)

```objc
[LADVideoPlayerManager fullScreenVideoPlayerWithItem:item configuration:configuration analyticsConfiguration:analyticsConfiguration completion:^(UIViewController *videoPlayerViewController) {
        // Present from your current view controller
    }];
```

- Get thumbnail video player:

![THUMBNAIL](https://gitlab.com/ladpods/ladplayermanager/raw/d313c20f3479d2322add3d35bd82295ea92e09b6/IMG_0040.PNG)

```objc
[LADVideoPlayerManager thumbnailVideoPlayerWithItem:item configuration:configuration analyticsConfiguration:analyticsConfiguration completion:^(UIView *videoPlayerView) {
        // Add in your video player content view
    }];
```

## Author

laddev, jean-baptiste.castro@lagardere-active.com

## License

LADPlayerManager is available under the MIT license. See the LICENSE file for more info.